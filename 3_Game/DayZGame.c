class dzr_cld_config_data_class
{
	//CONFIG
	bool	EnableDebug;
	float	CombatCheckRadius;
	string	DiscordUrl;
	bool	NotifyOnDiscord;
	string	DiscordCustomMessage;
	bool	ShowPenaltyDetailsInDiscord;
	bool	ShowPlayerUID;
	float	CombatModeCooldownTime_s;
	bool	ExitButtonNotify;
	
	
	string	PlayerCombatWarningTitle;
	string	PlayerCombatWarningText;
	string	PlayerCombatWarningIcon; 
	
	float	PenaltyTimer;
	
	float	PenaltyHealth;
	float	PenaltyBlood;
	float	PenaltyShock;
	
	float	PenaltyHunger;
	float	PenaltyThirst;
	
	bool	PenaltyRemoveGear;
	
	bool	PenaltyBloodyHands;
	bool	PenaltyBreakLegs;
	
	bool	KillInstantPenalty;
	string	KillWarningTitle="";
	string	KillWarningText="";
	string	KillWarningIcon="set:ccgui_enforce image:MapShieldBooster";
	
	bool	PenaltyTeleport;
	string	PenaltyTeleportCoords;
	string	PenaltyTeleportRotate;	
	
	ref array<string> PenaltyGiveItems;
	float	ItemsHealth;
	bool	BuildingLockdown;
	ref array<int> PenaltyDisieaseAgents;
	int DisieaseIntensity;
	
	bool	ExitButtonTrigger;
	
	bool	NamalskEVRStormTrigger;
	bool	NamalskBlizzardTrigger;
	
	bool	WeaponFireTrigger;
	bool	InfectedTrigger;
	bool	AnyDamageTrigger;
	
	bool	PredatorTrigger;
	bool	BloodlossTrigger;
	bool	BrokenLegsTrigger;
	
	string	README0;
	string	README1;
	string	README2;
	string	README3;
	string	README4;
	string	README44;
	string	README5;
	
	
	
	
	
	
	//string DisconnectAction;
	
	
	
	
	
	/*
		
		bool PenaltyRemoveWeapons;
		bool PenaltyRemoveAmmo;
		bool PenaltyBreakLegs;
		
		
		
	*/
	string	README6;
	
	
	string	Changelog;
	string	ModVersion;
	int		ConfigVersion;
	
	
	
	//CONFIG
	
	//default
	
	//CONFIG
	
	void dzr_cld_config_data_class (){
		
		EnableDebug = false; // Display diagnostic information.
		CombatCheckRadius		= 150;
		DiscordUrl = 			"";
		NotifyOnDiscord = 		false;
		DiscordCustomMessage=	"";
		ShowPenaltyDetailsInDiscord = 		true;
		ShowPlayerUID = 		true;
		CombatModeCooldownTime_s =	35;
		ExitButtonNotify =		true;
		
		PlayerCombatWarningTitle="";
		PlayerCombatWarningText="";
		PlayerCombatWarningIcon="set:ccgui_enforce image:MapShieldBooster";	
		
		
		
		PenaltyTimer =			15;
		
		PenaltyHealth =			80;
		PenaltyBlood = 			1500;
		PenaltyShock = 			82;
		
		PenaltyHunger = 		100;
		PenaltyThirst = 		100;
		
		PenaltyBreakLegs = 		false;
		PenaltyBloodyHands = 	true;
		
		PenaltyRemoveGear = 	true;
		
		KillInstantPenalty = 	true;
		KillWarningTitle="";
		KillWarningText="";
		KillWarningIcon=		"set:ccgui_enforce image:MapShieldBooster";
		
		PenaltyTeleport = 		true;
		PenaltyTeleportCoords = "2747.02 33.6652 1305.11";
		PenaltyTeleportRotate = "155 0 0";
		BuildingLockdown = 		true;
		
		PenaltyGiveItems = new array<string >;
		PenaltyGiveItems.Insert("PrisonUniformJacket");
		PenaltyGiveItems.Insert("PrisonUniformPants");
		PenaltyGiveItems.Insert("PrisonerCap");
		PenaltyGiveItems.Insert("WorkingBoots_Grey");
		
		ItemsHealth = 			0.0;
		
		/*
			const float DAMAGE_PRISTINE_VALUE 		= 1.0;
			const float DAMAGE_WORN_VALUE 			= 0.7;
			const float DAMAGE_DAMAGED_VALUE 		= 0.5;
			const float DAMAGE_BADLY_DAMAGED_VALUE 	= 0.3;
			const float DAMAGE_RUINED_VALUE 		= 0.0;
		*/
		
		PenaltyDisieaseAgents = new array<int>;
		
		PenaltyDisieaseAgents.Insert(eAgents.CHOLERA);
		PenaltyDisieaseAgents.Insert(eAgents.INFLUENZA);
		PenaltyDisieaseAgents.Insert(eAgents.SALMONELLA);
		PenaltyDisieaseAgents.Insert(eAgents.BRAIN);
		PenaltyDisieaseAgents.Insert(eAgents.FOOD_POISON);
		PenaltyDisieaseAgents.Insert(eAgents.WOUND_AGENT);
		PenaltyDisieaseAgents.Insert(eAgents.NERVE_AGENT);
		DisieaseIntensity = 100;
		
		NamalskEVRStormTrigger = true;
		NamalskBlizzardTrigger = true;
		
		
		WeaponFireTrigger = 	true;
		InfectedTrigger = 		true;
		AnyDamageTrigger = 		true;
		BrokenLegsTrigger = 	true;
		ExitButtonTrigger =		true;
		
		PredatorTrigger = 		false;
		BloodlossTrigger = 		false;
		
		README0 = 				"========================== README ==============================";
		README1 = 				"PenaltyTimer: If a player is in combat, this time will be added to logout timer.";
		README2 = 				"CombatCheckRadius: The radius of threat detection around the player. Bigger radius may cause freezes when logging out.";
		README3 = 				"DiscordUrl: To get notified in a chennel, you need to create a webhook for that channel - settings - integrations - create - copy URL";
		README4 = 				"NotifyOnDiscord: 0 to turn off, 1 to turn on notification in Discord.";
		README44 = 				"*****Trigger: All parameters ending with Trigger correspond to what is triggering the Combat Mode. Wepon firing, Infected agro, etc. PredatorTrigger and BloodlossTrigger are not implemented. PenaltyBreakLegs may not work too.";
		
		README5 = 				" ";//"=============Settings below are not implemented yet==============";
		README6 = 				"============== Debug information ===========";
		
		// Internal
		
		Changelog = "Many new features and triggers.";
		ModVersion = "1.4.41"; // If the mod config is updated and your version is different, your config will be backed up and the new config will be applied. You can restore your settings from your backup file manually.
		ConfigVersion = 59; // If the mod config is updated and your version is different, your config will be backed up and the new config will be applied. You can restore your settings from your backup file manually.
		
		// ========== Version number explanation ===========
		// M.m.R.C as in 1.0.5.44
		//M = Major version, like complete overhaul or engine change.
		//m = Minor version, important or multiple feature addition
		//R = Revision, updated after some fixes or tweaks applied.
		//C = Config structure changed, it wil not be compatible with older configs, so you will have to upgrade your config using your backed up file.
		
		//CONFIG
	}
	
}

modded class DayZGame {
	
	//string m_modName = "[DZR Combat Log Detection] ::: ";
	protected ref dzr_cld_config_data_class dzr_cld_config_data;
	
	dzr_cld_config_data_class ModConfig(string source = "not specified") {
		
		
		
		if(dzr_cld_config_data){
			this.Debug("ModConfig source: "+source, 0, dzr_cld_config_data.EnableDebug);
			return dzr_cld_config_data;
		}
		return null;
	}
	
	void CldSaveConfigOnServer(ref dzr_cld_config_data_class new_config) {
		//Print("[DZR Combat Log Detection] ::: DayZGame ::: SaveConfigOnServer");
		dzr_cld_config_data = new_config;
		//Print("[DZR Combat Log Detection] ::: DayZGame ::: SaveConfigOnServer");
	}	
	
	//ref dzr_cld_config_data_class m_CLDexistingConfig;
	
	void Debug(string strDebugMessage, int priority = 0, bool Enabled = false)
	{
		if(Enabled){
			string m_modName = "[DZR Combat Log Detection PlayerBase.c]";
			string m_side = "NULL";
			
			string delimiter = " ::::::::: ";
			
			if(priority == 1)
			{
				delimiter = " ■ ■ ■ ■ ■ ";	
			}
			if(priority == 2)
			{
				delimiter = " ■■■■■■■■■ ";
			}
			
			//PlayerBase theClient = this;
			
			
			
			if(GetGame().IsClient())
			{
				m_side = "Clientside";
			};
			
			if(GetGame().IsServer())
			{
				m_side = "Serverside";
			};
			int i_hour;
			int i_minute;
			int i_second;
			
			GetHourMinuteSecond(i_hour, i_minute, i_second);
			//GetYearMonthDay(i_year, i_month, i_day);
			string suffix = i_hour.ToString()+":"+i_minute.ToString()+":"+i_second.ToString();
			//if(theClient && theClient.GetIdentity())
			//{
			//GetGame().Chat(m_modName+delimiter+strDebugMessage, "colorImportant");
			Print("■"+suffix+"■ "+m_modName+delimiter+strDebugMessage+" ::: "+m_side);
			//Param1<string> Msg = new Param1<string>( strDebugMessage );
			//GetGame().RPCSingleParam( theClient, ERPCs.RPC_USER_ACTION_MESSAGE, Msg, true, theClient.GetIdentity() );
			//}
		}
	}
	
}
