modded class ZombieBase
{
	ref dzr_cld_config_data_class m_CLDexistingConfig;
	
	override void Init()
	{
		if(isServerSide())
		{
			m_CLDexistingConfig = GetDayZGame().ModConfig("ZombieBase");
			super.Init();
		}
	}
	
	
	bool getTarget(PlayerBase thisPlayer)
	{
		if(isServerSide())
		{
			PlayerBase playerTarget = PlayerBase.Cast(m_ActualTarget);
			if (thisPlayer == playerTarget && IsAlive() && playerTarget.GetIdentity())
			{
				string theMessage = playerTarget.GetIdentity().GetName()+ " ("+playerTarget.GetIdentity().GetPlainId()+") is chased by a zombie and now in Combat Mode for "+ (m_CLDexistingConfig.CombatModeCooldownTime_s) +" s.";
				//GetDayZGame().Debug(theMessage, 0, m_CLDexistingConfig.EnableDebug);
				
				return true;
			}
		}
		return false;
	}
	
	bool isServerSide()
	{
		if ( GetGame().IsMultiplayer() && GetGame().IsServer() )
		{
			if (GetGame().GetMission())
			{
				return true;
			}
		}
		return false;
	}
}