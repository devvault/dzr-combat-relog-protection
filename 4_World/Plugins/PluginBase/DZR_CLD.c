class CLDDiscordMessage3
{
	string username;
	string content;
	string avatar;
}

class DZR_CLD extends PluginBase
{
	dzr_cld_config_data_class	m_CLDexistingConfig;
	private PlayerBase 						m_Source;
	private BleedingSourcesManagerServer 	m_BleedMgr;	
	
	private string 		PlayerName;
	private string 		Victim;
	private string 		Killer;
	private string 		Message;
	private string 		VictimId;
	private string		KillerId;
	private string		KillWeapon;	
	private string 		KillMessage;
	private string 		DiscordMessage;
	private string 		DeathDetails;			
	private float 		m_Distance;
	private int 		kill_Distance;
	private int 		player_StatWater;
	private int 		player_StatEnergy;
	private string 		killTime;
	
	ref map<PlayerBase, bool> m_WaitingForPenalty = new map<PlayerBase, bool>;
	
	private bool StopDiscordMessage = true;
	private bool StopSideChatMessage = true;
	
	const int Player_Food 	 = 	200 ,Player_Water	 = 	300, Player_Blood	 =	3500 ;
	
	int i_year, i_month, i_day, i_hour, i_minute, i_second;
	string sYear, sMonth, sDay, sHour, sMinute, sSecond, currentTime, currentDate;	
	
	
	void DZR_CLD( dzr_cld_config_data_class externalConfig)
	{		
		if (GetGame().IsMultiplayer())
		{
			m_CLDexistingConfig = GetDayZGame().ModConfig("DZR_CLD.c");
			//Print("[DZR_CLD] :: Init");
		}	
	}
	
	
	void ~DZR_CLD()
	{
		if (GetGame().IsMultiplayer())
		{
			Print("[DZR_CLD] :: Closed");
		}
	}		
	
	string ReturnUtcTime()
	{			
		GetYearMonthDayUTC(i_year, i_month, i_day);
		GetHourMinuteSecondUTC(i_hour, i_minute, i_second);				
		sYear = i_year.ToString();
		sMonth = i_month.ToString();
		if (sMonth.Length()  == 1)		{			sMonth  = "0"   + sMonth;		}				sDay    = i_day.ToString();
		if (sDay.Length()	 == 1)		{			sDay    = "0"   + sDay;			}				sHour   = i_hour.ToString();
		if (sHour.Length()   == 1)		{			sHour   = "0"   + sHour;		}				sMinute = i_minute.ToString();
		if (sMinute.Length() == 1)		{			sMinute = "0"   + sMinute;		}				sSecond = i_second.ToString();
		if (sSecond.Length() == 1)		{			sSecond = "0"   + sSecond;		}						
		currentDate = 		sYear + "-" + sMonth  + "-" + sDay;
		currentTime = "T" + sHour + ":" + sMinute + ":" + sSecond + "Z";
		return currentDate + currentTime;
	}
	
	protected string GetPrefix(PlayerIdentity identity)  // Get player names
	{
		PlayerName = "Survivor (AI)";
		if (identity)
		{
			PlayerName = identity.GetName();
		}
		identity = NULL;		
		return PlayerName;
	}	
	
	void PlayerHit(PlayerBase player, EntityAI source)
	{
		m_CLDexistingConfig = GetDayZGame().ModConfig("DZR_CLD.c");
		if(m_CLDexistingConfig.AnyDamageTrigger)
		{
			if(source )
			{
				PlayerBase theAttacker = PlayerBase.Cast(source.GetHierarchyRootPlayer());
				if(theAttacker && theAttacker.IsPlayer())
				{
					if(theAttacker.IsPlayer())
					{
						theAttacker.setCombatMode(true, "EEHitBy");
						theAttacker.resetCombatTimer();
					}
				}
			}
			
			PlayerBase theVictim = player; //PlayerBase.Cast(GetHierarchyRootPlayer());
			
			//this.Debug("Player "+theVictim.GetIdentity().GetName() +" is hit by "+source);
			if(theVictim)
			{
				if(theVictim.IsPlayer() && theVictim.GetIdentity())
				{
					theVictim.setCombatMode(true);
					theVictim.resetCombatTimer();
					//string DebugMsg = the_killer.GetIdentity().GetName() +" killed other player and is in Combat Mode.";
					//this.Debug(DebugMsg);
				}
				
			}
		}
	}
	void PlayerKilled(PlayerBase player, Object source)
	{
		
		if (player && source)
		{												
			Print("[DZR_CLD] :: Player died and source was: " + source.GetDisplayName());
			Victim = this.GetPrefix( player.GetIdentity() );
			
			if( player.GetIdentity() && player.IsPlayer() )
			{
				VictimId = player.GetIdentity().GetPlainId();
			}
			KillMessage = "";
			StopSideChatMessage  = true;
			StopDiscordMessage   = true;
			
			if (player == source) // Player killed self, bled to death, starved, etc.
			{
				player_StatWater = Math.Round(player.GetStatWater().Get());
				player_StatEnergy = Math.Round(player.GetStatEnergy().Get());
				m_BleedMgr = player.GetBleedingManagerServer();
				
				
				StopDiscordMessage = false;
				StopSideChatMessage = false;
				if ( player_StatWater && player_StatEnergy && m_BleedMgr )
				{
					KillMessage=  Victim + " " + "Died";// Water: " + player_StatWater.ToString() + " Energy: " + player_StatEnergy.ToString() + " Bleed sources: " + m_BleedMgr.GetBleedingSourcesCount().ToString();
				}
				else if ( player_StatWater && player_StatEnergy && !m_BleedMgr )
				{
					KillMessage= Victim + " " + "Died";// Water: " + player_StatWater.ToString() + " Energy: " + player_StatEnergy.ToString();
				}
				else
				{
					KillMessage = Victim + " " + Victim + "UnknowMessages";
				}							
				
				DiscordMessage+= "Suicided" + "\"";
				DeathDetails = Victim + " " + "SelfDeath";					
				Print("[DZR_CLD] :: " + KillMessage);
				
			}
			else if (source.IsInherited(Grenade_Base)) // Player was killed by an explosion
			{
				
				StopDiscordMessage = false;
				StopSideChatMessage = false;
				KillWeapon = source.GetDisplayName();
				DiscordMessage+= KillWeapon + "\"";
				DeathDetails = "GrenadeMessages";							
				Print("[DZR_CLD] :: " + Victim +  "GrenadeMessages");							
				
				KillMessage = Victim + KillWeapon ;									
				
			}
			else if (source.IsInherited(TrapBase)) // Player was killed by a Trap
			{
				
				StopDiscordMessage = false;
				StopSideChatMessage = false;
				
				DeathDetails = " " + "LandMineMessages";						
				KillWeapon = source.GetDisplayName();			
				DiscordMessage+= KillWeapon + "\"";								
				Print("[DZR_CLD] :: " + Victim + "DiedToA" + KillWeapon);
				
				
				KillMessage = Victim + Victim + "DiedToA" + KillWeapon;						
				
			}
			else if ( source.IsWeapon() || source.IsMeleeWeapon() )  // player
			{				
				m_Source = PlayerBase.Cast( EntityAI.Cast( source ).GetHierarchyParent() );
				
				Killer = "";
				StopDiscordMessage = false;
				StopSideChatMessage = false;
				if (m_Source)
				{
					Killer = this.GetPrefix( m_Source.GetIdentity() );
					KillerId = m_Source.GetIdentity().GetPlainId();
					KillWeapon = source.GetDisplayName();
					
					
					DiscordMessage+= "" + Killer + "\"";
					
				}				
				if ( source.IsMeleeWeapon() )
				{	
					DeathDetails = "WBTDW" + KillWeapon;
					
					KillMessage = Victim + "KilledBy" + Killer + "With"+ " " + KillWeapon;										
				}
				else
				{
					m_Distance = vector.Distance( player.GetPosition(), m_Source.GetPosition() );
					kill_Distance = Math.Round(m_Distance);
					DeathDetails = "Weapon"+ " " + KillWeapon + " " + "Distance" + " " + kill_Distance + "M";
					KillMessage = Victim + "KilledBy" + Killer + "With" + KillWeapon + "From" + kill_Distance + "Meters";
				}
				
				m_CLDexistingConfig = GetDayZGame().ModConfig("DZR_CLD.c");
				if(m_CLDexistingConfig.KillInstantPenalty)
				{
					
					string warningTitle = "#STR_KILL_WARNING";
					string warningText = "#STR_KILL_WARNING_TEXT";
					
					if(m_CLDexistingConfig.KillWarningTitle != "")
					{
						warningTitle = m_CLDexistingConfig.KillWarningTitle;
					};
					if(m_CLDexistingConfig.KillWarningText != "")
					{
						warningText = m_CLDexistingConfig.KillWarningText;
					};
					
					this.DelayedPenalties(m_Source.GetIdentity(), m_Source, false, m_Source.GetIdentity().GetName(), m_Source.GetIdentity().GetPlainId(), 0, m_CLDexistingConfig);
					NotificationSystem.SendNotificationToPlayerIdentityExtended( m_Source.GetIdentity() , 10, warningTitle, warningText, m_CLDexistingConfig.KillWarningIcon);
				};
				
				Print("[DZR_CLD] :: " + KillMessage);
			}
			else if (source.IsInherited(DayZInfected)) // Player was killed by an Infected
			{
				
				StopDiscordMessage = false;
				StopSideChatMessage = false;
				Killer = source.GetDisplayName();					
				DeathDetails = Victim + " " + "ZombieKillMessages";
				DiscordMessage+=  "Zombie" + ": " + "ZombieNames" + "\"";					
				Print("[DZR_CLD] :: " + Victim + "WKB" + Killer);
				
				KillMessage = Victim + "WKB" + Killer;
				
				
			}
			else if (source.IsInherited(DayZAnimal)) // Player was killed by an animal
			{
				
				StopDiscordMessage = false;
				StopSideChatMessage = false;
				Killer = source.GetDisplayName();								
				DiscordMessage+= "" + Killer + "\"";
				DeathDetails = "AnimalMessages";					
				Print("[DZR_CLD] :: " + Victim + "WKB" + Killer);								
				
				
			}
			else if (source.IsInherited(CarScript)) // Player was run down by vehicle
			{
				
				StopDiscordMessage = false;
				StopSideChatMessage = false;
				DiscordMessage+= "Vehicle" + "\"";
				DeathDetails = Victim + " " + "VehicleMessages";				
				Print("[DZR_CLD] :: " + Victim + "VehicleMessages");									
				
				KillMessage = Victim + "WRDBAV";								
				
			}
			else // Player died of unknown cause
			{
				
				StopDiscordMessage = false;
				StopSideChatMessage = false;
				DiscordMessage+= "UnknowMessages" + "\"";
				DeathDetails = "UnknowMessages";
				Print("[DZR_CLD] :: " + Victim + "UnknowMessages");
				
				
				
				KillMessage = Victim + "UnknowMessages";
				
				
			}			
			
			if (!StopSideChatMessage)
			{
				
				//BroadCastKillToChat(KillMessage);
				
			}			
			if (!StopDiscordMessage)
			{
				
				DiscordMessage+= ",\"inline\":true},{\"name\":\"" + "Victim" +"\",\"value\":\"";									
				
				DiscordMessage+= "" + Victim + "\"";				
				
				DiscordMessage+= ",\"inline\":true},{\"name\":\"" + "KillDetails" + "\",\"value\":\"";
				DiscordMessage+= "" + DeathDetails + "\"";  // Kill details
				DiscordMessage+= "}],\"thumbnail\":{\"url\":\"";
				
				DiscordMessage+= "},\"timestamp\":\"";
				DiscordMessage+= "" + killTime +"\"}]}";
				Print(DiscordMessage);	
				DiscordMessage = "";
				
			}
		}
		
		
	}
	
	void SetHealthCB(PlayerBase p_player, string s_zone, string s_type, float f_value)
	{
		p_player.SetHealth(s_zone, s_type, f_value);
	}
	
	void DelayedPenalties(PlayerIdentity identity, PlayerBase player, bool authFailed, string name, string plainid, int logout, dzr_cld_config_data_class m_ExtCLDexistingConfig)
	{
		if(!player.isInCombat())
		{
			GetDayZGame().Debug("DZR_CLD.c player not in combat, cancelling", 0, m_ExtCLDexistingConfig.EnableDebug);
			return;
		};
		
		GetDayZGame().Debug("DelayedPenalties start from DZR_CLD.c", 0, m_ExtCLDexistingConfig.EnableDebug);
		if(m_ExtCLDexistingConfig.PenaltyRemoveGear)
		{
			player.stripPlayer();
		}
		
		if( m_ExtCLDexistingConfig.PenaltyGiveItems.Count() >= 1 )
		{
			EntityAI itemCreated = null;
			for ( int item_index = 0; item_index < m_ExtCLDexistingConfig.PenaltyGiveItems.Count(); item_index++ )
			{
				itemCreated = player.GetInventory().CreateInInventory(m_ExtCLDexistingConfig.PenaltyGiveItems[item_index]);
				itemCreated.SetHealth01("", "", m_ExtCLDexistingConfig.ItemsHealth);
			}
			
		}
		
		if(m_ExtCLDexistingConfig.PenaltyBreakLegs)
		{
			//GetGame().GetCallQueue(CALL_CATEGORY_GAMEPLAY).CallLater(player.SetBrokenLegs, 2000, false, eBrokenLegs.BROKEN_LEGS);
			player.SetBrokenLegs(eBrokenLegs.BROKEN_LEGS);
			
		};
		
		for ( int agent_index = 0; agent_index < m_ExtCLDexistingConfig.PenaltyDisieaseAgents.Count(); agent_index++ )
		{
			player.InsertAgent(m_ExtCLDexistingConfig.PenaltyDisieaseAgents[agent_index], m_ExtCLDexistingConfig.DisieaseIntensity);
		}
		
		if(m_ExtCLDexistingConfig.PenaltyTeleport)
		{
			player.SetPosition( m_ExtCLDexistingConfig.PenaltyTeleportCoords.ToVector() );
			player.SetOrientation( m_ExtCLDexistingConfig.PenaltyTeleportRotate.ToVector() );
			
			if( m_ExtCLDexistingConfig.BuildingLockdown )	
			{
				LockDoors(m_ExtCLDexistingConfig.PenaltyTeleportCoords.ToVector(), 50);
			}
		}
		
		
		
		float currentHealth = player.GetHealth("","");
		float currentBlood = player.GetHealth("","Blood");
		float currentShock = player.GetHealth("","Shock");
		player.SetHealth("", "", currentHealth - m_ExtCLDexistingConfig.PenaltyHealth);
		player.SetHealth("", "Blood", currentBlood - m_ExtCLDexistingConfig.PenaltyBlood);
		player.SetHealth("", "Shock", currentShock - m_ExtCLDexistingConfig.PenaltyShock);
		
		player.GetStatEnergy().Add( -m_ExtCLDexistingConfig.PenaltyHunger );
		player.GetStatWater().Add( -m_ExtCLDexistingConfig.PenaltyThirst );
		
		player.SetBloodyHands( m_ExtCLDexistingConfig.PenaltyBloodyHands);
		
		GetGame().GetCallQueue(CALL_CATEGORY_GAMEPLAY).CallLater(SetHealthCB, 3000, false, "", "Shock", currentShock - m_ExtCLDexistingConfig.PenaltyShock);
		//player.SetHealth("", "Shock", currentShock - m_ExtCLDexistingConfig.PenaltyShock);
		string extraText;
		
		if(m_ExtCLDexistingConfig.NotifyOnDiscord)
		{
			if(currentHealth <= m_ExtCLDexistingConfig.PenaltyHealth)
			{
				extraText = "\r\n Current health"+": "+currentHealth+". :skull_crossbones: "+"Character will probably die after the penalty.";
			}
			
			string d_Message = "";
			string theUid = "";
			if(m_ExtCLDexistingConfig.ShowPlayerUID)
			{
				theUid = " ("+plainid+")";
			}
			
			
			
			if(m_ExtCLDexistingConfig.ShowPenaltyDetailsInDiscord)
			{
				if(m_ExtCLDexistingConfig.DiscordCustomMessage != "")
				{
					d_Message += m_ExtCLDexistingConfig.DiscordCustomMessage+" ["+"Player"+" "+name+theUid+ "]\r\n";
				}
				{
					//d_Message += ":warning: **"+"Player"+" "+name+theUid+ "  "+"disconnected in Combat Mode and received the following penalties:"+"**";
					d_Message += string.Format(":warning: ** %1 "+name+theUid+ "  %2**", "Player", "disconnected in Combat Mode and received the following penalties:");
				}
				
				if(m_ExtCLDexistingConfig.PenaltyTimer != 0){
					d_Message += "\r\n:hourglass_flowing_sand: Logout was delayed for **"+m_ExtCLDexistingConfig.PenaltyTimer+" sec.**";
				};
				
				if(m_ExtCLDexistingConfig.PenaltyHealth != 0){
					d_Message += "\r\n:broken_heart: Health **-"+m_ExtCLDexistingConfig.PenaltyHealth+"."+extraText+"**";
				};
				if(m_ExtCLDexistingConfig.PenaltyBlood != 0){
					d_Message += "\r\n:drop_of_blood: Blood **-"+m_ExtCLDexistingConfig.PenaltyBlood+"**";
				}
				if(m_ExtCLDexistingConfig.PenaltyShock != 0){
					d_Message += "\r\n:anger: Shock **-"+m_ExtCLDexistingConfig.PenaltyShock+"**";
				}
				if(m_ExtCLDexistingConfig.PenaltyHunger != 0){
					d_Message += "\r\n:apple: Energy **-"+m_ExtCLDexistingConfig.PenaltyHunger+"**";
				}
				if(m_ExtCLDexistingConfig.PenaltyThirst != 0){
					d_Message += "\r\n:droplet: Water **-"+m_ExtCLDexistingConfig.PenaltyThirst+"**";
				}
				if(m_ExtCLDexistingConfig.PenaltyBloodyHands){
					d_Message += "\r\n:palms_up_together: Bloody hands";
				}
				if(m_ExtCLDexistingConfig.PenaltyBreakLegs){
					d_Message += "\r\n:wheelchair: Broken legs";
				}
				if(m_ExtCLDexistingConfig.PenaltyDisieaseAgents.Count() >= 1){
					d_Message += "\r\n:microbe: Ifections added: **"+m_ExtCLDexistingConfig.PenaltyDisieaseAgents.Count()+"**";
				}
				if(m_ExtCLDexistingConfig.PenaltyGiveItems.Count() >= 1){
					d_Message += "\r\n:gift: Items added: **"+m_ExtCLDexistingConfig.PenaltyGiveItems.Count()+"**";
				}
				if(m_ExtCLDexistingConfig.KillInstantPenalty){
					d_Message += "\r\n:oncoming_police_car: Player kill penalty";
				}
				if(m_ExtCLDexistingConfig.PenaltyTeleport){
					d_Message += "\r\n:flying_saucer: Teleported";
				}
				if(m_ExtCLDexistingConfig.PenaltyRemoveGear){
					d_Message += "\r\n:customs: All gear removed";
				}
				else
				{
					d_Message += ":warning: Player **"+name+theUid+ " disconnected in Combat Mode and received the following penalties:**";
				}
				
			};
			sendToDiscord(d_Message, m_ExtCLDexistingConfig.DiscordUrl);
			//Print(identity.GetPlainId()+" disconnected while in Combat Mode. received -"+m_CLDexistingConfig.PenaltyHealth+" health penalty."+extraText);
			GetDayZGame().Debug("___________________"+player+" Penalized_________________________");
		}
		
		
		//isWaitingForPenalty(player, "DelayedPenalties END");
	}
	
	
	//Print("authFailed2: "+authFailed);
	
	
	void Debug(string strDebugMessage, int priority = 0, bool Enabled = false)
	{
		if(Enabled){
			string m_modName = "[DZR Combat Log Detection DZR_CLD.c]";
			string m_side = "NULL";
			
			string delimiter = " ::::::::: ";
			
			if(priority == 1)
			{
				delimiter = " ■ ■ ■ ■ ■ ";	
			}
			if(priority == 2)
			{
				delimiter = " ■■■■■■■■■ ";
			}
			
			//PlayerBase theClient = this;
			
			PlayerBase theClient = PlayerBase.Cast( GetGame().GetPlayer() );
			
			if(GetGame().IsClient())
			{
				m_side = "Clientside";
			};
			
			if(GetGame().IsServer())
			{
				m_side = "Serverside";
			};
			
			int i_hour;
			int i_minute;
			int i_second;
			
			GetHourMinuteSecond(i_hour, i_minute, i_second);
			//GetYearMonthDay(i_year, i_month, i_day);
			string suffix = i_hour.ToString()+":"+i_minute.ToString()+":"+i_second.ToString();
			
			if(theClient && theClient.GetIdentity())
			{
				GetGame().Chat(m_modName+delimiter+strDebugMessage, "colorImportant");
				Print("■"+suffix+"■ "+m_modName+delimiter+strDebugMessage+" ::: "+m_side);
				Param1<string> Msg = new Param1<string>( strDebugMessage );
				GetGame().RPCSingleParam( theClient, ERPCs.RPC_USER_ACTION_MESSAGE, Msg, true, theClient.GetIdentity() );
			}
		}
	}
	//Lock doors
	void LockDoors(vector position, int radius)
	{    
		
		
		vector pos = position;
		ref array<Object> nearest_objects = new array<Object>;
		ref array<CargoBase> proxy_cargos = new array<CargoBase>;
		GetGame().GetObjectsAtPosition ( pos, radius, nearest_objects, proxy_cargos );
		
		for ( int i = 0; i < nearest_objects.Count(); i++ )
		{
			Object nearest_object = nearest_objects.Get(i);
			
			if ( nearest_object.IsInherited( Building ) )
			{    
				
				Building building = Building.Cast( nearest_object );
				int doorsCount = building.GetGame().ConfigGetChildrenCount("CfgVehicles " + building.GetType() + " Doors");                
				for (int c = 0; c < doorsCount; c++)
				{
					//vector distanceRoot = vector.DistanceSq(player.GetPosition(), playerRootPos);
					//if( this.IsInReach(player, target, radius) && !building.IsDoorOpen(c) ) 
					//{
					if ( building.IsDoorOpen(c) ) building.CloseDoor(c);
					building.LockDoor(c);
					//}
					
					
				}
			}    
		}
	}
	void sendToDiscord(string message, string webhookURL)
	{
		if(m_CLDexistingConfig.NotifyOnDiscord)
		{
			// Compose the message to be sent to Discord.
			CLDDiscordMessage3 m_CLDDiscordMessage = new CLDDiscordMessage3();
			m_CLDDiscordMessage.username = "Combat Log Detection";
			m_CLDDiscordMessage.content = message;
			m_CLDDiscordMessage.avatar = "https://cdn.discordapp.com/attachments/948492094564089906/948848483157307432/unknown.png";
			
			// Convert that message into JSON format.
			string discordMessageJSON;
			JsonSerializer jsonSerializer = new JsonSerializer();
			jsonSerializer.WriteToString(m_CLDDiscordMessage, false, discordMessageJSON);
			//https://cdn.discordapp.com/attachments/948492094564089906/948848483157307432/unknown.png
			// Post the message to Discord.
			private RestApi restAPI;
			private RestContext restContext;
			webhookURL.Replace("https://discord.com/api/webhooks/", "");
			
			if(GetGame().IsServer())
			{
				restAPI = CreateRestApi();
				restContext = restAPI.GetRestContext("https://discord.com/api/webhooks/");
				restContext.SetHeader("application/json");
				restContext.POST(NULL, webhookURL, discordMessageJSON);
				
				//GetDayZGame().Debug("m_CLDexistingConfig.NotifyOnDiscord: " + m_CLDDiscordMessage.content);
			}
		}
	}
}