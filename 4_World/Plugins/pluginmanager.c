modded class PluginManager extends PluginManager
{
    override void Init()
    {
        super.Init();
        
		if (GetGame().IsServer() && GetGame().IsMultiplayer())
		{
			Print("[DZR_CLD] :: Loading Server Plugin");
			RegisterPlugin("DZR_CLD", false, true);
		}
	}
}